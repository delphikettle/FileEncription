#include "Encryption.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int encryptChar(char *c, struct PublicKeys *pc);

int powmod(long a, long k, long n);

char decryptChar(int *c, struct PrivateKeys *pc);

char *encryptString(char *str, struct PublicKeys *pc);

char *decryptString(char *str, struct PrivateKeys *pc);

int encryptFile(char *fileName, struct PublicKeys *pc);


int decryptFile(char *fileName, struct PrivateKeys *pc);
int encryptChar(char *c, struct PublicKeys *pc) {
    return powmod(*c, pc->e, pc->n);
}

char decryptChar(int *c, struct PrivateKeys *pc) {
    return (char) powmod(*c, pc->d, pc->n);
}



int powmod(long a, long k, long n) {
    long b = 1;
    long k_ = k;
    while (k_) {
        if (k_ % 2 == 0) {
            k_ /= 2;
            a = (a * a) % n;
        } else {
            k_--;
            b = (b * a) % n;
        }
    }
    return (int) b;
}

char *encryptString(char *str, struct PublicKeys *pc) {
    printf("Encryption of \"%s\" started\n", str);
    int n = (int) strlen(str);
    printf("lenght=%d\n", n);
    int x, c;
    char *newStr = (char *) calloc(sizeof(int) * n + 1, sizeof(char));
    for (int i = 0; i < n; ++i) {
        c = (int) str[i];
        c = (c > 0 ? c : c + 256);
        char cc = c;
        x = encryptChar(&cc, pc);
        printf("%c(%d)=%d:\n", c, c, x);
        for (int j = 3; j >= 0; --j) {
            newStr[4 * i + j] = (char) (1 + x % 256);
            x = x / 256;
        }
    }
    newStr[4 * n] = '\0';
    printf("Encryption finished with \"%s\"\n", newStr);
    return newStr;
}

char *decryptString(char *str, struct PrivateKeys *pc) {
    printf("Decryption of \"%s\" started\n", str);
    int n = (int) strlen(str);
    printf("lenght=%d\n", n);
    int newN = n / sizeof(int) + /*(n % sizeof(int) ? sizeof(int) : 0)*/+1;
    printf("newLenght=%d\n", newN);
    int x, c;
    char *newStr = (char *) calloc(newN, sizeof(char));
    for (int i = 0; i < newN - 1; ++i) {
        x = 0;
        for (int j = 0; j < 4; j++) {
            x = x * 256;
            c = str[4 * i + j];
            x = x + (c > 0 ? c : c + 256) - 1;
        }

        newStr[i] = decryptChar(&x, pc);
        printf("%c=%d\n", newStr[i], x);
    }
    newStr[newN] = '\0';
    printf("Decryption finished with \"%s\"\n", newStr);
    return newStr;
}

int encryptFile(char *fileName, struct PublicKeys *pc) {
    printf("Encryption of \"%s\" file started\n", fileName);
    FILE *fp = NULL, *fpR = NULL;
    char newFileName[128];
    strcpy(newFileName, fileName);
    strcat(newFileName, ".enc");
    fp = fopen(fileName, "r");
    fpR = fopen(newFileName, "w");
    int x, c;
    if (fp == NULL)
        return 1;
    char *mas = (char *) calloc(1, 4);
    while (!feof(fp)) {
        c = fgetc(fp);
        c = (c > 0 ? c : c + 256);
        char cc = (char) c;
        x = encryptChar(&cc, pc);
        //printf("%c(%d)=%d:\n", c, c, x);
        for (int j = 3; j >= 0; --j) {
            mas[j] = (char) (1 + x % 256);
            //printf(" %c(%d)\n", (char) (1 + x % 256), (1 + x % 256));
            x = x / 256;
        }
        for (int i = 0; i < 4; ++i) {
            fputc(mas[i], fpR);
        }
    }
    free(mas);
    fclose(fp);
    fclose(fpR);
    printf("Encryption of \"%s\" file finished\n", fileName);
    return 0;
}

int decryptFile(char *fileName, struct PrivateKeys *pc) {
    printf("Decryption of \"%s\" file started\n", fileName);
    FILE *fp = NULL, *fpR = NULL;
    char newFileName[128];
    strcpy(newFileName, fileName);
    strcat(newFileName, ".dec");
    fp = fopen(fileName, "r");
    fpR = fopen(newFileName, "w");
    int x, c;
    if (fp == NULL)
        return 1;
    int symNo = 0;
    while (!feof(fp)) {
        c = fgetc(fp);
        symNo++;
    }
    int symI = 0;
    fclose(fp);
    fp = fopen(fileName, "r");
    while (!feof(fp)) {
        x = 0;
        for (int j = 0; j < 4; j++) {
            //printf(" x1=%d", x);
            x = x * 256;
            //printf(" x2=%d", x);
            c = fgetc(fp);
            symI++;
            c = (c > 0 ? c : c + 256);
            x = x + c - 1;
            //printf(" x3=%d", x);
            //printf(" %c(%d)\n", c, c);
        }
        if (symI >= symNo - 1)
            continue;

        fputc(decryptChar(&x, pc), fpR);
        //printf(":%c(%d)=%d\n", (char) decryptChar(&x, pc), (int) decryptChar(&x, pc), x);
    }
    fclose(fp);
    fclose(fpR);
    printf("Decryption of \"%s\" file finished\n", fileName);
    return 0;
}

