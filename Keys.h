//
// Created by dk on 28.10.16.
//

#ifndef FILEENCRIPTION_KEYS_H
#define FILEENCRIPTION_KEYS_H

struct Keys {
    unsigned int p, q;
    unsigned int n, fi, e, d;
};
struct PublicKeys {
    unsigned int e, n;
};
struct PrivateKeys {
    unsigned int d, n;
};

int generateKeys(struct Keys *keys);

unsigned int getEByFi(unsigned int fi);

unsigned int getDByFiAndE(unsigned int fi, unsigned int e);

struct PublicKeys getPublicKeys(struct Keys *keys);

struct PrivateKeys getPrivateKeys(struct Keys *keys);

#endif //FILEENCRIPTION_KEYS_H
