
#include "Keys.h"

#ifndef FILEENCRIPTION_ENCRYPTION_H
#define FILEENCRIPTION_ENCRYPTION_H

int encryptChar(char *c, struct PublicKeys *pc);

int powmod(long a, long k, long n);

char decryptChar(int *c, struct PrivateKeys *pc);

char * encryptString(char *str, struct PublicKeys *pc);

char * decryptString(char *str, struct PrivateKeys *pc);

int encryptFile(char *fileName, struct PublicKeys *pc);

int decryptFile(char *fileName, struct PrivateKeys *pc);

#endif //FILEENCRIPTION_ENCRYPTION_H
